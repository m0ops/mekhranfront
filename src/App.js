import React, { Component } from 'react';
import './App.css';
import Content from './Content.js'

class App extends Component {
  render() {
    return (
      <div className="view-info left">
        <div className="container">
            <Content />
        </div>
      </div>
    );
  }
}

export default App;
