import React from "react";
import './LeftviewInfo.css';

function renderRow(item) {

    if(item[0] !== "netfac_set")
    {
        let ftitle = item[0]
        let fvalue = item[1]

        if(fvalue === false) {fvalue = 'false'}
        if(fvalue === true){fvalue = 'true'}

        return (
            <div key={item[0]} className='view-row'>
                <div className='field-title'>{ftitle}</div>
                <div className='field-value'>{fvalue}</div>
            </div>
        )
    }
}

export function GeneralInfo(dataInfo) {
    return(
           Object.entries(dataInfo.data.data).map(item => {return renderRow(item)})
    )
}
