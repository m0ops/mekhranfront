import React from 'react';
import './PrivatePeeringFacilities.css';

function renderRow(fPlatform, fCity, locAsn, id) {
        return (
            <div key={id} className='view-row'>
                <div className="left-field-item">
                    <div className='field-platform'>{fPlatform}</div>
                    <div className='field-local-asn'>{locAsn}</div>
                </div>
                <div className="right-field-item">
                    <div className='field-city'>{fCity}</div>
                </div>
            </div>
        )
}

export function PrivatePeeringFacilities(dataInfo){

    return(
        <div className='list'>
            <div className="filter"> </div>
            <div className='list-header'>
                <div className='Asn'>
                    <div>Площадка</div>
                    <div>ASN</div>
                </div>
                <div className='Country'>
                    <div>Страна</div>
                    <div>Город</div>
                </div>
            </div>
            { generateRows(dataInfo.data) }
        </div>
    )
}

function generateRows(data) {
    let Rows = [];
    for (let key in data.netfac_set) {
        let fPlatform = data.netfac_set[key].fac.name
        let fCity = data.netfac_set[key].fac.city
        let locAsn = data.netfac_set[key].local_asn
        let id =  data.netfac_set[key].id

        Rows.push(renderRow(fPlatform, fCity, locAsn, id));
    }
    return Rows;
}



