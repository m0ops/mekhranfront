import React, { Component } from 'react';
import {LeftView, RightView} from './ViewInfo.js'
import './Content.css';
import axios from 'axios';


export default class Content extends Component {
    state = {
        loaded: false,
        Data: []
    };

    componentDidMount() {
        axios.get(`http://178.128.196.163:3000/api/records/`)
            .then(res => {
                const AllData = res.data;
                if (AllData.length !== 0) {
                    const Data = AllData[0].data.results[0];
                    this.setState({Data})
                    this.setState({loaded : true})
                }
            })
    }

    render() {
        if (!this.state.loaded) { return (<p>Loading...</p>); }
        return (
            <div className='view'>
                <div className='row page-header'>{this.state.Data.name}</div>
                <div className='row'>
                    <LeftView data={this.state.Data}/>
                    <RightView data={this.state.Data}/>
                </div>
            </div>
        )
    }
}



