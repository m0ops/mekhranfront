import React from 'react';
import './ViewInfo.css';
import {PrivatePeeringFacilities} from './PrivatePeeringFacilities.js'
import {GeneralInfo} from './LeftViewInfo'

export function RightView(dataInfo) {
    return(
        <PrivatePeeringFacilities data={dataInfo.data}/>
    )
}

export function LeftView(dataInfo) {
    return(
        <div className='view-field'>
            <GeneralInfo data={dataInfo}/>
        </div>
    )
}

